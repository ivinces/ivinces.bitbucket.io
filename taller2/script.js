function addTweet(titulo, descripcion, link, fecha,imagen){
	var title= $("<h5/>",{
		"class":"card-title",
		html: titulo
	})

	var texto= $("<p/>",{
		"class":"card-text",
		html:descripcion
	})

	var lin= $("<a/>",{
		"class": "col-12 card-text",
		"href": link, 
		html:link
	});
	

	var date=$("<p/>",{
		"class":"card-text",
		html:fecha,
		"id":"fecha"
	})

	var img=$('<img/>',{
		"class":"col-2",
		"src":imagen,
		"id":"logo"
	});

	var div=$("<div/>",{
		"class":"card-body",
		"id":"tweet"
	});

	var row=$("<div/>",{
		"class":"row"
	});

	var div2=$("<div/>",{
		"class":"col-10"
	});

	img.appendTo(row)
	title.appendTo(div2)
	texto.appendTo(div2)
	lin.appendTo(div2)
	div2.appendTo(row)
	row.appendTo(div)
	date.appendTo(div)
	div.appendTo("#tweets");
}

function loadTweetsXml(){
	$.ajax({
		type: "GET",
		url: "tweets.xml",
		dataType:"xml",
		success: function(xml){
			$(xml).find('item').each(function(){
				var titulo=$(this).find('title').text();
				var descripcion=$(this).find('description').text();
				var link=$(this).find('link').text();
				var fecha=$(this).find('pubDate').text();
				var imagen=$(xml).find('url').text();
				addTweet(titulo,descripcion,link,fecha,imagen);
			});
		},
		error: function(){
			alert("Error al procesar el xml");
		}
	});
}

$(document).ready(function(){
	loadTweetsXml();

	$("button").click(function(e){
		e.preventDefault();

    var texto = $('input#buscador').val();
    $('h2#texto').text(texto);
    
    if(texto.length != 0) {
    
      $('#tweets .card-body').filter(function(index){
        
        $(this).show();
        
        var twt = $(this).text()
        if(twt.indexOf(texto) == -1) {
          $(this).hide()
        }

      });

    } else {
      $('#tweets .card-body').each(function(){
        $(this).show();
      });
    }
  })

});